<?php
/**
 * Created by PhpStorm.
 * User: dsnopek
 * Date: 2/16/17
 * Time: 6:59 PM
 */

namespace Drupal\pixelhangers\Plugin\Block;


use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *   id = "pixel_hangers_block",
 *   admin_label = @Translation("Pixel Hangers Block")
 * )
 */
class PixelHangersBlock extends BlockBase {

  public function build() {
    $build['block']['#markup'] = '<p>Pixel Hangers is AWesome!</p>';
    return $build;
  }

}